const Web3 = require('web3');
const fs = require('fs');
const settings = require('../web3.config.json');

const web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(settings.HttpProvider));

const contractsFiles = fs.readdirSync('contracts/sources');

const solContracts = contractsFiles
  .map(fileName => fs.readFileSync(`contracts/sources/${fileName}`, 'utf-8'));

const contracts = solContracts
  .map(solContract => web3.eth.compile.solidity(solContract))
  .map((contract, index) => {
    const contractName = contractsFiles[index].match(/(.*).sol/)[1];
    const gasEstimate = web3.eth.estimateGas({
      data: JSON.stringify(contract.info)
    });

    return {
      name: contractName,
      abi: contract.info.abiDefinition,
      address: null,
      gasEstimate,
      code: contract.code
    }
  });

contracts
  .forEach(contract => {
    const rawContract = JSON.stringify(contract, null, 2);
    fs.writeFileSync(
      `contracts/${contract.name}.json`,
      rawContract,
      'utf-8'
    )
  })
