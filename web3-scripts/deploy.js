const Web3 = require('web3');
const fs = require('fs');
const settings = require('../web3.config.json');

const web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(settings.HttpProvider));

// This is the account the contracts will be deployed from.
const mainAccount = web3.eth.accounts[0];
web3.eth.defaultAccount = web3.eth.coinbase;

const contractsFiles = fs
  .readdirSync('contracts')
  .filter(file => file !== 'sources'); // Exclude sources folder from list

const contracts = contractsFiles
  .map(fileName => fs.readFileSync(`contracts/${fileName}`, 'utf-8'))
  .map(rawContract => JSON.parse(rawContract));

/**
 * When creating a contract, its callback is called twice.
 * Once the contract has the transactionHash property set and once its deployed on an address.
 * 
 * But there is a bug, the second callback raises an error exception.
 * It says that the contract code couldn't be stored, even if the contract has been successfully deployed!.
 * 
 * To get around this, we create promises that will send back the transactionHash.
 * With this transactionHash, we can get the contract address within the transactionReceipts.
 */
const promises = contracts.map(contract => {
  return new Promise(res => {
    const web3Contract = web3.eth.contract(contract.abi);
    const contractInstance = web3Contract.new({
      from: mainAccount,
      gas: contract.gasEstimate,
      code: contract.code
    }, (err, i) => {
      console.log(err);

      
      /**
       * How it should really work, but an error is fired before the 2nd
       * callback
       */
      if (i && i.address) res(i);

      /**
       * Workaround : resolve the promise and use transactionHash
       */
      res(i);
    });
  })
});

Promise.all(promises).then(contractsDeployed => {
  contractsDeployed.forEach(({ transactionHash, abi }, index) => {
    // We get the contract address thanks to the transactionHash.
    const contractAddress = web3.eth
      .getTransactionReceipt(transactionHash)
      .contractAddress;

    // We store the address into the new contract JSON file that can be accessed
    // into our client.
    const contract = Object.assign({}, contracts[index], {
      address: contractAddress
    });

    const rawContract = JSON.stringify(contract, null, 2);
    fs.writeFileSync(
      `contracts/${contract.name}.json`,
      rawContract,
      'utf-8'
    )
  })
});
