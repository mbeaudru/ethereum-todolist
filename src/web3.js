import Web3 from 'web3';
import TodoList from '../contracts/TodoList.json';

const web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider('http://localhost:8545'));

web3.eth.defaultAccount = web3.eth.accounts[0];

const contractInstance = web3.eth.contract(TodoList.abi).at(TodoList.address);

/**
 * Should be equal to 100, returns 0
 */
console.log(contractInstance.double(50).toString());

/**
 * In testrpc, transaction is sent properly
 */
contractInstance.addTodoItem('Todo item added');

/**
 * Call fired in testrpc console, always return empty todolist.
 * Should return previously added item.
 */
console.log(contractInstance.getTodoItems());

export default web3;

export const setAccount = (address) => {
  web3.eth.defaultAccount = address;
};

export const getEthBalance = (account) =>
  web3.toWei(web3.eth.getBalance(account), "ether").toString();

export const getContractInstance = (abi, address) =>
  web3.eth.contract(abi).at(address);